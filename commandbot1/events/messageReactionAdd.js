
module.exports = async (reaction, user) => {
  if(reaction.message.partial) await reaction.message.fetch();
  if (reaction.partial) await reaction.fetch();

  const client = user.client;
  const connection = client.connection;
   let modlog = client.channels.cache.get('417638357959573505');


  //lfr-staff reactions
  if(reaction.message.id == "710591765194735616"){
      var role = "";
      if(reaction._emoji.id == "651156619824070687"){ //cox
        role = "cox";
      }else if(reaction._emoji.id == "710582107994325132"){ //cm
        role = "cm";
      }else if(reaction._emoji.id == "702205226681237505"){ //tob
        role = "tob";
      }
      else if(reaction._emoji.id == "675667021157105694") { //Nightmare
        role = "nm"
      }
    var sql_command = "INSERT INTO WDR.LFR_STAFF (discord_id, activity) VALUES (\'" + user.id + "\',\'" + role + "\')";
    connection.query(sql_command, function(error, result, fields){
      if(error) { throw error }
      reaction.message.guild.member(user.id).send("lfr-staff " + role + " added")
    });
    return;
  }
  //modmail reactions
  if(reaction.message.channel.id === modlog.id){
    if(reaction.message.reactions.length > 1){
      return;
    }
    var uid = user.id;
    if(reaction._emoji.name === "❌")
      uid = "-1"
    var sql_command_modmail = "UPDATE WDR.MODMAIL SET user_reacted = '"+uid+"', modmail_reacted_time = '"+ new Date().toISOString().slice(0, 19).replace('T', ' ') +"'  WHERE message_id = '"+reaction.message.id+"' AND user_reacted IS NULL;";
    connection.query(sql_command_modmail, function(error, result, fields){
      if(error) { throw error }
      if(result.affectedRows > 0){
        if(reaction._emoji.name === "❌"){
          var sql_command_modmail_2 = "SELECT mm.user_modmailed FROM WDR.MODMAIL mm WHERE message_id = '"+reaction.message.id+"'";
          connection.query(sql_command_modmail_2, async function(error, result, fields) {
            if (error) {
              throw error
            }
            try {
              var userid = result[0].user_modmailed
              var user = await reaction.client.users.fetch(userid)
              await user.send("Your latest modmail lacks some essential information. Please resubmit it with the following:\n" +
                "<:cox:651156619824070687> A description of what happened.\n" +
                "<:cox:651156619824070687> Names and discord accounts of everybody involved.\n" +
                "<:cox:651156619824070687> Links to non-embedded screenshots or other evidence.")
            } catch (e) {
              if(e.name == "DiscordAPIError"){
                reaction.message.channel.send("error `"+ e.message + "` occured on the following modmail: \nhttps://discordapp.com/channels/408082929940430858/417638357959573505/"+reaction.message.id);
              }
            }
          });
        }
      }
    });
    return;
  }

  //verzik-verification reactions
  if (reaction.message.channel.id == "556879235059548161"){ //only mentor+ has perms in that channel anyway, bad practice but I accidentally fucked up the entire bot
    if(reaction._emoji.name === "✅"){

      let verzik = await reaction.message.guild.roles.fetch("556557558761127936");
      let user = await reaction.message.guild.members.fetch(reaction.message.author);
      if (!user) {
        console.log("something went wrong with fetching the user")
        return;
      }
      await user.roles.add(verzik).catch(console.error);
      reaction.message.delete();
    }
    // if(reaction._emoji.name === "❌") {
    //   reaction.message.delete();
    // }

  }

  if (reaction.message.channel.id == "744028112454549574"){ //only mentor+ has perms in that channel anyway, bad practice but I accidentally fucked up the entire bot
    if(reaction._emoji.name === "✅"){
      let coxadv = await reaction.message.guild.roles.fetch("744026872467161108");
      let user = await reaction.message.guild.members.fetch(reaction.message.author);
      if (!user) {
        console.log("something went wrong with fetching the user")
        return;
      }
      await user.roles.add(coxadv).catch(console.error);
      reaction.message.delete();
    }
  }
};
