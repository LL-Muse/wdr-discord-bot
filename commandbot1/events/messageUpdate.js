const invitefilter = require('../util/invitefilter.js');
module.exports = async (oldMessage, newMessage) => {
  if(newMessage.partial) await newMessage.fetch();

  if (newMessage.author.bot) return; //this is just here in case there's more we want to add in the future.
  if(!newMessage.guild) return;

  let client = newMessage.client;
  let perms = client.elevation(newMessage);

  if(invitefilter(newMessage) && perms < permLevel.TRIALMOD) {
    newMessage.member.send("It looks like you linked to a Discord that is not on our whitelist. If you think this was in error, please message a mod or admin. You can find our whitelist here: <https://docs.google.com/spreadsheets/d/1NO7WJg2Jb983O28mEAnNZTc5Fgnioy2ee3GKbHpU1nQ>");
    newMessage.delete();
    return;
  }
};