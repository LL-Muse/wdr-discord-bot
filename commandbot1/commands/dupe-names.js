exports.run = async function(client, message, args) {

  if(message.channel.id == 417638357959573505) { //mod chat
    message.channel.send("You can't execute that command here.")
    return;
  }
  let memberMap = new Map();
  var members = message.guild.members.cache;
  for(var member of members) {
    var user = message.guild.members.cache.get(member[1].user.id);
    var name = user.nickname;
    if (name == null || name == undefined) {//use discord name if no sever nickname
      name = member[1].user.username;
    }
    var userNames = new Array();
    if(name.includes("(")){
      name = name.split('(')[0]
    }
    if(name.includes("[")){
      name = name.split('[')[0]
    }
    if(name.includes("|")){
      userNames = name.split('|');
    }
    else{
      userNames = [name];
    }
    for(let rsn of userNames) {
      rsn = rsn.trim();
      if(rsn.length == 0)
        continue;
      // console.log(rsn);
      if (memberMap.has(rsn)) {
        memberMap.get(rsn).occurences++
        memberMap.get(rsn).userId += ","+member[1].user.id;
      }
      else{
        memberMap.set(rsn, {occurences: 1, userId: member[1].user.id})
      }
    }
  }
  let mapFiltered = new Map([...memberMap].filter(([k, v]) => v.occurences > 1));

  message.channel.send("Dupe names found: "+mapFiltered.size);

  for(var dupeuser of mapFiltered.values()){
    userids = dupeuser.userId.split(',')
    var pings = userids.map(el => "<@!"+el+">");
    message.channel.send(pings.join(" "));
  }
  message.channel.send("done.");

}

exports.conf = {
  enabled: true,
  guildOnly: false,
  aliases: [],
  permLevel: permLevel.TRIALMOD
};

exports.help = {
  name: 'dupe-names',
  description: 'dupe-names',
  usage: 'dupe-names'
};
