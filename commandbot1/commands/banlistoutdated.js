exports.run = async function(client, message, args){

    const {google} = require("googleapis");
	const google_sheet_keys = require("/root/wdrbot/google_sheet_keys.json");
	const fetch = require("node-fetch");

    const connection = client.connection;

	const google_client = new google.auth.JWT(
        google_sheet_keys.client_email,
        null,
        google_sheet_keys.private_key,
        ["https://www.googleapis.com/auth/spreadsheets"]
    );

    google_client.authorize(function(error, tokens){
        if(error){
            console.log(error);
            return;
        }
    });

    const google_sheet = google.sheets({version:"v4", auth:google_client})
    const opt= {
        spreadsheetId: "1RCSAGdCyef2h2whTga6Q5uCAyEh3ffU9WOKmaryl2gs",
        range: "Data"
    }
    var data = await google_sheet.spreadsheets.values.get(opt);
    var banlist = data.data.values;

    var outdated = [];

    for(var i = 1; i < banlist.length; i++){
        if(banlist[i][0] == undefined || banlist[i][0] == null || banlist[i][0] == ""){
            continue;
        }
        var lookup = await getStats(banlist[i][0].trim());
        if(lookup.real){
            var name = banlist[i][0];
            if(args[0] != undefined && args.includes("-v")){
                message.channel.send(name + " -- up to date");    
            }
            if(args.includes("-o")){
                var sql_command_4 = "DELETE FROM WDR.BANNED_USER_STATS WHERE rsn=\'" + name + "\'";
                await connection.query(sql_command_4, function(error, result, fields){
                    if(error){ throw error }
                })
                var sql_command_5 = "INSERT INTO WDR.BANNED_USER_STATS (rsn, stats, last_update) VALUES (\'" + name + "\',\'" + JSON.stringify(lookup.data) + "\',\'" + new Date().toISOString().slice(0, 19).replace('T', ' ') + "\')";
                await connection.query(sql_command_5, function(error, result, fields){
                    if(error){ throw error }
                })
            }
        }else{
			var name = banlist[i][0];
      var sql_command_1 = "SELECT * FROM WDR.BANLIST_OUTDATED WHERE name =\'" + name + "\'";
			await connection.query(sql_command_1, async function(error, result, fields){
                if(error){ throw error }
                outdated.push(name);
                if(result[0] != undefined && result[0] != null){
                    if(args[0] != undefined && args.includes("-v")){
                        await message.channel.send("```\n" + name + " -- outdated(old)" + "\n```");
                    }

                }else{
                    await message.channel.send("```\n" + name + " -- outdated(recent change)" + "\n```");
                }
            })
        }
    }

	var sql_command_2 = "DELETE FROM WDR.BANLIST_OUTDATED";
	await connection.query(sql_command_2, function(error, result, fields){
 		 if(error){ throw error }
 	})
 	for(var i = 0; i < outdated.length; i++){
 		sql_command_3 = "INSERT INTO WDR.BANLIST_OUTDATED (name) VALUES (\"" + outdated[i] + "\");"
 		await connection.query(sql_command_3, function(error, result, fields){
 			if(error){ throw error }
 		})
 	}
 	
    message.channel.send("banlist outdated check done");

    async function getStats(rsn) {
        const response = await fetch("http://services.runescape.com/m=hiscore_oldschool/index_lite.ws?player=" + encodeURIComponent(rsn));
        const text = await response.text();
        if(!text.includes("<title>404 - Page not found</title>")){
            return ({"real":true, "data": parseStatsData(text)})
        }else{
            return({"real":false, "data":null});
        }
    }

    function parseStatsData (data) {
        const skills = ['overall','attack','defence','strength','hitpoints','ranged','prayer','magic','cooking','woodcutting','fletching','fishing','firemaking','crafting','smithing','mining','herblore','agility','thieving','slayer','farming','runecraft','hunter','construction'];
        var sortedData = {};
        var splitData = data.split('\n');
        for (var i = 0; i < skills.length; i++) {
            var level = splitData[i].split(',')[1];
            var exp = splitData[i].split(',')[2];
            sortedData[skills[i]] = {"level" : level, "exp" : exp};
        }
        return sortedData;
    }
};

exports.conf = {
  enabled: true,
  guildOnly: false,
  aliases: [],
  permLevel: permLevel.TRIALMOD
};

exports.help = {
  name: 'banlist-outdated',
  description: 'staff only',
  usage: 'staff only'
};
