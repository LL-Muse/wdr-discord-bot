const Discord = require("discord.js");
exports.run = function(client, message){

  const updatePlayerCount = require ("../util/updatePlayerCount.js");

  updatePlayerCount(client, message);

  let total = message.guild.memberCount;
  let nitro = message.guild.roles.cache.get("585528815229599775").members.size;

  const embed = new Discord.MessageEmbed()
    .setTitle(`${message.guild.name}`)
    .setThumbnail(`${message.guild.iconURL()}`)
    .addField("Member Count", total)
    .addField("Role Count", `${message.guild.roles.cache.size}`)
    .addField("Voice Channels", `${message.guild.channels.cache.filter(m => m.type ==="voice").size}`)
    .addField("Text Channels", `${message.guild.channels.cache.filter(m => m.type ==="text").size}`)
    .addField("Nitro Boosters", nitro, true);

  //message.guild.roles.cache.get('132123');
  //but if you want id with bot, just find it by name, and then add a .id and log it / send i
  message.channel.send({embed});
};

exports.conf = {
  enabled: true,
  guildOnly: false,
  aliases: ['si'],
  permLevel: permLevel.INACTIVEMENTOR
};

exports.help = {
  name: "serverinfo",
  description: "Various counts",
  usage: "serverinfo"
};
