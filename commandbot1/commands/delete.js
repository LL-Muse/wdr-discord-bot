exports.run = function(client, message, args){

  let user = message.mentions.users.first();
  let messageCount = parseInt(args.slice(1).join(" "));

  console.log(messageCount)
  if(messageCount > 1) message.channel.messages.fetch({
    limit: messageCount
  }).then(messages => {
    var temp =messages.filter(m => m.author.id === user.id);
    if(temp.size>1){
      message.channel.bulkDelete(temp);
    }
    else{
      temp.first().delete();
    }
  });
}

exports.conf = {
  enabled: true,
  guildOnly: false,
  aliases: [],
  permLevel: permLevel.TRIALMOD
};

exports.help = {
  name: 'delete',
  description: 'Delete messages from a specific user',
  usage: 'delete <mention> <number>'
};
