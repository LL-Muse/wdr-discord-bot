exports.run = function(client, message){
    var output_string = "";
    
    // User must be in channel
  	if (!message.member.voice.channel) {
        message.channel.send(`You must be in the voice channel to use this command`);
        return;
  	}

  	let vc = message.member.voice.channel;
  	
    // Command must contain target channel ID
    let tokens = message.content.split("|");
    if (tokens.length != 2) {
	    message.channel.send(`Invalid Usage`);
	    return;
	}
 
  	let members = vc.members.array();

  	for (let i = 0; i < members.length; i++) {
    	output_string = output_string + "<@!" + members[i].id + "> ";
    } 

    message.channel.send(output_string + tokens[1]);
};

exports.conf = {
  enabled: true,
  guildOnly: false,
  aliases: ["vc"],
  permLevel: permLevel.INACTIVEMENTOR
};

exports.help = {
  name: "ping-vc",
  description: "Pings everyone in your current voice channel",
  usage: 'USAGE: !ping-vc | message'
};
