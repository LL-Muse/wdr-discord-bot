exports.run = function(client, message, args){
    const connection = client.connection;
  var mes = "";
  var role = "";
  if(args[0].toLowerCase() == "cox"){
    role = "cox";
    mes+= "CoX ";
  }else if(args[0].toLowerCase() == "tob"){
    role = "tob";
    mes+= "ToB ";
  }else if(args[0].toLowerCase() == "cm"){
    role = "cm";
    mes+= "CM "
  }else if(args[0].toLowerCase() == "nm") {
    role = "nm";
    mes += "Nightmare "
  }else{
    return;
  }
  var sql_command = "SELECT DISTINCT discord_id FROM WDR.LFR_STAFF WHERE activity=\'" + role + "\'";
  connection.query(sql_command, function(error, result, fields){
    if(error) { throw error }

    for(var i = 0; i < result.length; i++){
      mes += "<@!" + result[i].discord_id + "> ";
    }
    message.channel.send(mes);
  });
};

exports.conf = {
  enabled: true,
  guildOnly: false,
  aliases: ['lfrs'],
  permLevel: permLevel.INACTIVEMENTOR
};

exports.help = {
  name: 'lfr-staff',
  description: '',
  usage: ''
};

