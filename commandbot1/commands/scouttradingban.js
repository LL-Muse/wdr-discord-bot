exports.run = async function(client, message, args) {

  let users = message.mentions.members;
  var user_ids = Array.from(users.keys());
  if(user_ids.length > 1 || user_ids.length === 0){
    message.channel.send("Please ping exactly 1 user to ban from scout trading");
    return;
  }

  var userId = user_ids[0];

  var st = client.channels.cache.get("530829059048079360");

  await st.updateOverwrite(userId, {'VIEW_CHANNEL': false});

  message.channel.send("<@!"+userId+"> has been banned from "+st.toString());

  let offences = message.guild.channels.cache.find(channel => channel.id === "408119551566282752");

  let offence = "Bad scouts in scout trading";
  let punishment = "Scout trading ban";
  let totalStrikes = "N/A";

  let msg = "**Discord Name:** <@!"+userId+">\n";//single line break
  msg += "**Discord ID:** "+userId+"\n\n";//Double line break here
  msg += "**Offence:** "+offence+"\n\n"; //Another double line break required here
  msg += "**Punishment:** "+punishment+"\n"; //Single line break
  msg += "**Total Strikes:** "+totalStrikes+"\n"; //One more line break
  msg += "**Issued by:** <@!"+message.member.id+">\n";
  msg += "━━━━━━━━━━━━━━━━━━";

  offences.send(msg);


}
exports.conf = {
  enabled: true,
  guildOnly: false,
  aliases: ["stb"],
  permLevel: permLevel.TRIALMOD
};

exports.help = {
  name: 'scouttradingban',
  description: 'Bans a user from scout trading',
  usage: 'stb @user'
};
