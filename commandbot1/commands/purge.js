exports.run = function(client, message, args){

  let messageCount = parseInt(args.join(" "));

  if(messageCount > 1) message.channel.messages.fetch({
    limit: messageCount
  }).then(messages => { //                          Official Account ID                      Bot ID                  
    var temp = messages.filter(m => (m.author.id != '408276491072307202') || (m.author.id !='408384918763208714'));
    if(temp.size>1){
      message.channel.bulkDelete(temp);
    }
    else{
      temp.first().delete();
    }
  });
}

exports.conf = {
  enabled: true,
  guildOnly: false,
  aliases: [],
  permLevel: permLevel.TRIALMOD
};

exports.help = {
  name: 'purge',
  description: 'Delete X amount of messages from the channel',
  usage: 'purge <number>'
};
