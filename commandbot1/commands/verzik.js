exports.run = function(client, message){
  let user = message.mentions.members.first();
  let verzik = message.guild.roles.cache.get("556557558761127936");
  if(!user) return message.delete().then(message.reply('You must have a valid mention.'));
  user.roles.add(verzik).catch(console.error);
};

exports.conf = {
  enabled: true,
  guildOnly: false,
  aliases: ['v'],
  permLevel: permLevel.INACTIVEMENTOR
};

exports.help = {
  name: 'verzik',
  description: 'Give verzik role to pinged',
  usage: 'verzik [PING USER]'
};
