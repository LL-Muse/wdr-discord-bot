const Discord = require("discord.js");
const moment = require('moment');
exports.run = function(client, message, args){
   
    let user;
    if (message.mentions.users.first()) {
        user = message.mentions.users.first();
    } else {
        user = message.author;
    }
    
    const member = message.guild.member(user);
    
    const embed = new Discord.MessageEmbed()
        .setColor('#283680')
        .setThumbnail(user.avatarURL)
        .addField(`${user.tag}`, `${user}`, true)
        .addField("ID:", `${user.id}`, true)
        .addField("Nickname:", `${member.nickname !== null ? `${member.nickname}` : 'None'}`, true)
        .addField("Joined The Server On:", `${moment.utc(member.joinedAt).format("dddd, MMMM Do YYYY")}`, true)
        .addField("Account Created On:", `${moment.utc(user.createdAt).format("dddd, MMMM Do YYYY")}`, true)
        .addField("\u200B", "\u200B")
        .addField("Roles:", member.roles.cache.map(roles => `${roles}`).join(', '), true)
        .setFooter(`Replying to ${message.author.username}#${message.author.discriminator}`)
    message.channel.send({embed});

}
exports.conf = {
  enabled: true,
  guildOnly: false,
  aliases: ['ui'],
  permLevel: permLevel.MENTOR
};

exports.help = {
  name: 'userinfo', 
  description: 'userinfo',
  usage: 'userinfo'
};
