exports.run = function(client, message){

  let user = message.mentions.members.first();
  
  let app_role = message.guild.roles.cache.get("572481959083638815"); //cox-appliacant
  let cox_learner = message.guild.roles.cache.get("644528392418623488");

  var nickname = user.nickname;
  if (nickname == null || nickname == undefined) {//use discord name if no sever nickname
      nickname = message.guild.member(user.id).user.username;
  }

  let perms = [
    { //channel invisable to plebs
      id : message.guild.roles.everyone,
      deny : ['VIEW_CHANNEL']
    },
    {//applicant can see their channel
      id : user.id, 
      allow: ['VIEW_CHANNEL'],
      deny : ['SEND_MESSAGES', 'ADD_REACTIONS']
    },
    {//admin bot
      id : "482771653273714688",
      allow: ['VIEW_CHANNEL']
    },
    {//mod
      id: "408106863117336577",
      allow: ['VIEW_CHANNEL']
    },
    {//cox mentor
      id: "520749698424766475",
      allow: ['VIEW_CHANNEL', 'MANAGE_MESSAGES']
    },
    {//normal mentor
      id: "408107637100511243",
      allow: ['VIEW_CHANNEL', 'MANAGE_MESSAGES']
    },
    {//trial mod
      id: "423653735449886720",
      allow: ['VIEW_CHANNEL'],
      deny: ['MANAGE_ROLES', 'MANAGE_CHANNELS']
    }
  ];

  if(!user) return message.delete().then(message.reply('You must have a valid mention.'));
  
  //add role  
  user.roles.add(app_role).catch(console.error);
  user.roles.add(cox_learner).catch(console.error);
  //get position
  var channel = message.guild.channels.cache.get('634465573509791754'); //Divider
  var position = channel.rawPosition;
  //create channel
  let messageId = -1;
  var channel_name = "cox-" + (nickname.replace(/ /g, "-"));
  message.guild.channels.create(channel_name, {type: 'text', parent:'471165503528894464', permissionOverwrites:perms, position:--position}).then(function (channel){
    channel.send("",{files: ["https://cdn.discordapp.com/attachments/601910003871383582/601910004559380480/combats_title.png"]}).then(function (message){messageId = message.id})
      .then(() => channel.send("**__Guardians__**").then(function (message){message.react("❌")}))
      .then(() => channel.send("**__Mystics__**").then(function (message){message.react("❌")}))
      .then(() => channel.send("**__Shamans__**").then(function (message){message.react("❌")}))
      .then(() => channel.send("**__Muttadiles__**").then(function (message){message.react("❌")}))
      .then(() => channel.send("**__Vanguards__**").then(function (message){message.react("❌")}))
      .then(() => channel.send("**__Vespula__**").then(function (message){message.react("❌")}))
      .then(() => channel.send("**__Tekton__**").then(function (message){message.react("❌")}))
      .then(() => channel.send("**__Vasa__**").then(function (message){message.react("❌")}))
      .then(() => channel.send("",{files: ["https://cdn.discordapp.com/attachments/601885803328831500/601885834248978473/puzzles_title.png"]}))
      .then(() => channel.send("**__Tightrope__**").then(function (message){message.react("❌")}))
      .then(() => channel.send("**__Ice Demon__**").then(function (message){message.react("❌")}))
      .then(() => channel.send("**__Crabs__**").then(function (message){message.react("❌")}))
      .then(() => channel.send("**__Thieving__**").then(function (message){message.react("❌")}))
      .then(() => channel.send("",{files: ["https://cdn.discordapp.com/attachments/601885803328831500/601885861042323486/olm_title.png"]}))
      .then(() => channel.send("**__Prep__**").then(function (message){message.react("❌")}))
      .then(() => channel.send("**__Speech__**").then(function (message){message.react("❌")}))
      .then(() => channel.send("**__Fight__**").then(function (message){message.react("❌")}))
      .then(() => channel.send("",{files: ["https://cdn.discordapp.com/attachments/605996560161177613/605996671108907029/wdr_line.png"]}))
      .then(() => channel.send("<@!" + user.id + ">"))
      .then(() => channel.send("This is the channel that mentors will use to post their trial reviews of you throughout your trial process. There are three steps to this application:\n" +
        "\n" +
        "<:cox:651156619824070687> You will ride along with mentors during mentor raids and observe them.\n" +
        "\n" +
        "<:cox:651156619824070687> You will do a pretrial raid in which you teach and a mentor observes. After this raid, you are eligible for [Trial Mentor] rank should you complete a raid without very serious concerns.\n" +
        "\n" +
        "<:cox:651156619824070687> You will complete trial raids until you successfully pass all rooms including all aspects of Olm, at which point the mentors will vote on your promotion. You may be asked to complete more trials just to be sure that you are ready for promotion. \n" +
        "\n" +
        "The necessary raids info regarding trialing is found in <#512843903980929054> and <#599009888030687232> - we will expect a complete understanding of this material. You are expected to make good progress on your trials throughout each week. If you will be gone for an extended period of time during your trials, please let the mentor team know. *You have one month to complete your trials.*\n" +
        "\n" +
        "You have been given the 'CoX Applicant' role which gives you access to <#440659803841363968> along with 'CoX Learner' to be pinged for mentor raids advertised in #mentor-raid-times.\n" +
        "\n" +
        "**Please use the `ping-role CT | (message here)` command to alert the mentors when you are ready to trial/ridealong.** Please have a scout ready when you do this. You should trial with as many different mentors as possible to ensure consistency in your feedback - your first five trials must each be with a different mentor. Do not be hesitant to use this to ping the team to let them know you are online and available. Feel free to let any mentors or mods know if you have any questions."))
      .then(() => channel.send("",{files: ["https://cdn.discordapp.com/attachments/605996560161177613/605996671108907029/wdr_line.png"]}))
      .then(() => channel.messages.fetch(messageId).then(function (message) {message.pin()}));
  });
};

exports.conf = {
  enabled: true,
  guildOnly: false,
  aliases: [],
  permLevel: permLevel.INACTIVEMENTOR
};

exports.help = {
  name: 'cox-channel',
  description: 'Create CoX Applicant channel',
  usage: 'cox-channel'
};
