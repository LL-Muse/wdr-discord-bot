exports.run = async function(client, message, args){

    const fetch = require("node-fetch");

    var name = args.join(" ");
    if(name == undefined || name == null || name == ""){
        message.channel.send("must provide rsn");
        return;
    }

    var lookup = await getStats(name);
    if(lookup.real){
		format_kc(lookup.data, message)
    }else{
        message.channel.send("rsn not found on hiscore");
    }


function format_kc (data, message){
        var ret = "```";
        const offset_1 = 17;
        const offset_2 = 6;
        const offset_3 = 8;
        const bosses = ["Abyssal Sire", "Alchemical Hydra", "Barrows Chests", "Bryophyta", "Callisto", "Cerberus", "Chambers of Xeric", "CoX CM", "Chaos Elemental", "Chaos Fanatic", "Commander Zilyana", "Corporeal Beast", "Crazy Arch", "Dagannoth Prime", "Dagannoth Rex", "Dagannoth Supreme", "Deranged Arch", "General Graardor", "Giant Mole" , "Dusk & Dawn", "Hespori", "Kalphite Queen", "King Black Dragon", "Kraken", "Kree'Arra", "K'ril Tsutsaroth", "Mimic", "Nightmare", "Obor", "Sarachnis", "Scorpia", "Skotizo", "Gauntlet", "Corrupt Gauntlet", "Theatre of Blood", "Thermy", "TzKal-Zuk", "TzTok-Jad", "Venenatis", "Vet'ion", "Vorkath", "Wintertodt", "Zalcano", "Zulrah"];
        ret = ret + "╔" + repeatchar("═", offset_1 + 2) + "╦" + repeatchar("═", offset_2) + "╦" + repeatchar("═", offset_3) + "╗\n";
        ret = ret + "║ " + center_fill_with_spaces("boss", offset_1) + " ║" + center_fill_with_spaces("kc", offset_2) + "║" + center_fill_with_spaces("rank", offset_3) + "║\n";
        ret = ret + "╠" + repeatchar("═", offset_1 + 2) + "╬" + repeatchar("═", offset_2) + "╬" + repeatchar("═", offset_3) + "╣\n";
        for(var i = 0; i < bosses.length; i++){
            ret = ret + "║ " + left_fill_with_spaces(bosses[i], offset_1) + " ║" + right_fill_with_spaces(data[bosses[i]].kc, offset_2) + "║" + right_fill_with_spaces(data[bosses[i]].rank, offset_3) + "║\n";
        }   
        ret = ret + "╚" + repeatchar("═", offset_1 + 2) + "╩" + repeatchar("═", offset_2) + "╩" + repeatchar("═", offset_3) + "╝";
        ret = ret + "```";    
        message.channel.send(ret);
    }

    function right_fill_with_spaces (mes, length){
        var ret = mes;
        while(ret.length < length){
            ret = " " + ret;
        }
        return ret;
    }

    function left_fill_with_spaces (mes, length){
        var ret = mes;
        while(ret.length < length){
            ret = ret + " ";
        }
        return ret;
    }

    function center_fill_with_spaces (mes, length){
        var ret = mes;
        while(ret.length < length - 1){
            ret = " " + ret + " ";
        }
        if(ret.length < length){
            ret = ret + " ";
        }
        return ret;
    }

    function repeatchar(char, amount){
        var ret = "";
        while(ret.length < amount){
        ret += char;
        }
        return ret;
    }

    async function getStats(rsn) {
        const response = await fetch("http://services.runescape.com/m=hiscore_oldschool/index_lite.ws?player=" + encodeURIComponent(rsn));
        const text = await response.text();
        if(!text.includes("<title>404 - Page not found</title>")){
            return ({"real":true, "data": parseKCData(text)})
        }else{
            return({"real":false, "data":null});
        }
    }

    function parseKCData (data) {
        const bosses = ["Abyssal Sire", "Alchemical Hydra", "Barrows Chests", "Bryophyta", "Callisto", "Cerberus", "Chambers of Xeric", "CoX CM", "Chaos Elemental", "Chaos Fanatic", "Commander Zilyana", "Corporeal Beast", "Crazy Arch", "Dagannoth Prime", "Dagannoth Rex", "Dagannoth Supreme", "Deranged Arch", "General Graardor", "Giant Mole" , "Dusk & Dawn", "Hespori", "Kalphite Queen", "King Black Dragon", "Kraken", "Kree'Arra", "K'ril Tsutsaroth", "Mimic", "Nightmare", "Obor", "Sarachnis", "Scorpia", "Skotizo", "Gauntlet", "Corrupt Gauntlet", "Theatre of Blood", "Thermy", "TzKal-Zuk", "TzTok-Jad", "Venenatis", "Vet'ion", "Vorkath", "Wintertodt", "Zalcano", "Zulrah"];
        const offset = 35;
        var sortedData = {};
        var splitData = data.split('\n');

        for (var i = offset; i - offset < bosses.length; i++) {
            var kc = splitData[i].split(',')[1];
            if(kc == "-1"){ kc = "0"}
            var rank = splitData[i].split(',')[0];
            if(rank == "-1"){ rank = "unranked"}
            sortedData[bosses[i - offset]] = {"kc" : kc, "rank" : rank};
        }
        return sortedData;
    }
};

exports.conf = {
  enabled: true,
  guildOnly: false,
  aliases: [],
  permLevel: permLevel.EVERYONE
};

exports.help = {
  name: 'kc',
  description: 'shows boss kcs',
  usage: 'kc rsn'
};

