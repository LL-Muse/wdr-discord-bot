const CANVAS = require('canvas')
const fs = require('fs');

exports.run = function(client, message, args){
  let connection = client.connection;

  if (args[0] !== undefined){
    if (args[0].toLowerCase() === "undo") {
      var undo_sql_command = "DELETE FROM WDR.MENTOR_RAID_LOGS WHERE WDR.MENTOR_RAID_LOGS.user_id = \'" + message.author.id + "\' ORDER BY WDR.MENTOR_RAID_LOGS.msg_time DESC LIMIT 1";
      connection.query(undo_sql_command, async function (error, result, fields) {
        if (error) {
          message.channel.send("```json\n" + JSON.stringify(error) + "\n```");
          throw error
        }
        if(result.affectedRows < 1){
          message.author.send("There were no raids to be removed");
        }
        else{
          message.author.send("last logged raid has been removed.");
        }
      });

      return;
    }
  }
  const num_to_month = {
    0 : "January",
    1 : "February",
    2 : "March",
    3 : "April",
    4 : "May",
    5 : "June",
    6 : "July",
    7 : "August",
    8 : "September",
    9 : "October",
    10 : "November",
    11 : "December"
  }

  var date = new Date();
  var this_month = date.getMonth() + 1;
  var next_month = this_month + 1;
  var this_year = 1900 + date.getYear();
  var next_month_year = this_year;
  if (next_month == 13){
    next_month = 1;
    next_month_year++;
  }
  if(this_month < 10){
    this_month = "0" +this_month;
  }
  if(next_month < 10){
    next_month = "0" + next_month;
  }

  var unsorted_data;

  var sql_command = "SELECT WDR.MENTOR_RAID_LOGS.user_id AS id, WDR.MENTOR_IDS.user_discord_name AS rsn, WDR.MENTOR_RAID_LOGS.message AS raid_type, count(*) as total_count FROM WDR.MENTOR_RAID_LOGS JOIN WDR.MENTOR_IDS ON WDR.MENTOR_RAID_LOGS.user_id=WDR.MENTOR_IDS.user_id AND (msg_time BETWEEN '" + this_year + "-" + this_month + "-01 00:00:00' AND '" + next_month_year + "-" + next_month + "-01 00:00:00') GROUP BY WDR.MENTOR_RAID_LOGS.user_id, WDR.MENTOR_RAID_LOGS.message";
  connection.query(sql_command, async function(error, result, fields){
    if(error){
      message.channel.send("```json\n"+JSON.stringify(error)+"\n```");
      throw error
    }
    if(result != undefined && result != null){
      unsorted_data = result;

      //shorten names
      for(var i = 0; i < unsorted_data.length; i++){
        if(unsorted_data[i].rsn.includes("[")){
          unsorted_data[i].rsn = unsorted_data[i].rsn.split("[")[0];
        }
        if(unsorted_data[i].rsn.includes("|")){
          unsorted_data[i].rsn = unsorted_data[i].rsn.split("|")[0];
        }
        if(unsorted_data[i].rsn.includes("(")){
          unsorted_data[i].rsn = unsorted_data[i].rsn.split("(")[0];
        }
      }

      //combine tob + cox for same person
      var cox_total = 0;
      var tob_total = 0;
      var sorting = {};
      for(var i = 0; i < unsorted_data.length; i++){
        if(sorting[unsorted_data[i].rsn] == undefined){
          sorting[unsorted_data[i].rsn] = {
            "rsn" : unsorted_data[i].rsn,
            "tob" : 0,
            "cox" : 0,
            "cm" : 0,
            "adv" : 0,
            "total" : 0
          }
        }
        if(unsorted_data[i].raid_type == "cox"){
          sorting[unsorted_data[i].rsn].cox += unsorted_data[i].total_count;
          sorting[unsorted_data[i].rsn].total += unsorted_data[i].total_count;
          cox_total += unsorted_data[i].total_count;
        }else if(unsorted_data[i].raid_type == "cm"){
          sorting[unsorted_data[i].rsn].cm += unsorted_data[i].total_count;
          sorting[unsorted_data[i].rsn].total += unsorted_data[i].total_count;
          cox_total += unsorted_data[i].total_count;
        }else if(unsorted_data[i].raid_type == "adv"){
          sorting[unsorted_data[i].rsn].adv += unsorted_data[i].total_count;
          sorting[unsorted_data[i].rsn].total += unsorted_data[i].total_count;
          cox_total += unsorted_data[i].total_count;
        }
        else if(unsorted_data[i].raid_type == "tob"){
          sorting[unsorted_data[i].rsn].tob += unsorted_data[i].total_count;
          sorting[unsorted_data[i].rsn].total += unsorted_data[i].total_count;
          tob_total += unsorted_data[i].total_count;
        }
      }
      var data = Object.values(sorting);

      //sort data by toal raids
      function compare(a, b) {
        if (a.total > b.total) return 1;
        if (b.total > a.total) return -1;
        return 0;
      }
      data.sort(compare);

      //graph properies
      const bar_cox_color = "green";
      const bar_adv_color = "lime";
      const bar_cm_color = "teal";
      const bar_tob_color = "red";
      const grid_color_1 = "white";
      const grid_color_2 = "AliceBlue";
      const text_color = "white";
      const yeetline_color = "black";

      const x_axis_offset = 200;
      const y_axis_offset = 200;
      const bar_height = 20;
      const bar_spacing = 5;
      const canvas_width = 2000;
      const canvas_height = y_axis_offset + data.length * (bar_spacing + bar_height);

      const canvas = CANVAS.createCanvas(canvas_width, canvas_height);
      const ctx = canvas.getContext('2d');

      //background
      ctx.fillStyle = "#37393F";
      ctx.fillRect(0, 0, canvas_width, canvas_height);

      //title
      ctx.font = '100px serif';
      ctx.textAlign = "center";
      ctx.fillStyle = text_color;
      ctx.fillText('Mentor Raid Log ' + num_to_month[date.getMonth()] + " " + (date.getYear() + 1900), canvas_width / 2, 100);


      //gridlines
      var max = find_max(data);
      var interval_amount = closest_round_number(max/10);
      var num_interval = Math.floor(max/interval_amount) + 1;
      var interval_pixel_distance = (canvas_width - x_axis_offset) / num_interval;
      var pixel_per_amount = interval_pixel_distance/interval_amount;
      for(var i = 0; i < num_interval; i++){
        if(i % 2 == 0){
          ctx.fillStyle = grid_color_1;
        }else{
          ctx.fillStyle = grid_color_2;
        }
        ctx.fillRect(x_axis_offset + (i * interval_pixel_distance), y_axis_offset, interval_pixel_distance, (canvas_height - y_axis_offset));
      }

      //bars

      for(var i = 0; i < data.length; i++){
        ctx.fillStyle = bar_cox_color;
        ctx.fillRect(x_axis_offset, y_axis_offset + (i*(bar_height+bar_spacing)), data[i].cox * pixel_per_amount, bar_height);
        ctx.fillStyle = bar_adv_color;
        ctx.fillRect(x_axis_offset + (data[i].cox * pixel_per_amount), y_axis_offset + (i*(bar_height+bar_spacing)), data[i].adv * pixel_per_amount, bar_height);
        ctx.fillStyle = bar_cm_color;
        ctx.fillRect(x_axis_offset + ((data[i].cox + data[i].adv)* pixel_per_amount), y_axis_offset + (i*(bar_height+bar_spacing)), data[i].cm * pixel_per_amount, bar_height);
        ctx.fillStyle = bar_tob_color;
        ctx.fillRect(x_axis_offset + ((data[i].cox + data[i].adv + data[i].cm) * pixel_per_amount), y_axis_offset + (i*(bar_height+bar_spacing)), data[i].tob * pixel_per_amount, bar_height);
      }

      //yeet line
      ctx.fillStyle = yeetline_color;
      ctx.fillRect((x_axis_offset+(pixel_per_amount*4))-5, y_axis_offset, 5, data.length*(bar_height+bar_spacing));

      //yeet line label
      ctx.font = '20px serif';
      ctx.textAlign = "center";
      ctx.fillStyle = text_color;
      ctx.fillText("4", (x_axis_offset+(pixel_per_amount*4)), y_axis_offset-5);


      //x axis labels
      ctx.font = '20px serif';
      ctx.textAlign = "center";
      ctx.fillStyle = text_color;
      for(var i = 0; i < num_interval; i++){
        ctx.fillText(i * interval_amount, x_axis_offset + (i * interval_pixel_distance), y_axis_offset-5);
      }

      //y axis labels
      ctx.font = '20px serif';
      ctx.textAlign = "right";
      ctx.fillStyle = text_color;
      for(var i = 0; i < data.length; i++){
        ctx.fillText(data[i].rsn, x_axis_offset - 10 , y_axis_offset + 12 + (i * (bar_height+bar_spacing)));
      }

      //write file and upload to discord
      const out = fs.createWriteStream('test.png')
      const stream = canvas.createPNGStream()
      stream.pipe(out)
      out.on('finish', () =>  message.channel.send("", { files: ["./test.png"] }))
      message.channel.send("red = tob\ngreen = cox (teal = cm,lime = adv)\ntob total: " + tob_total + "\ncox total: " + cox_total + " (including cm and adv)");
    }
  });

  function closest_round_number(x){
    var ret = 1;
    while(x > 10){
      x = x/10;
      ret = ret*10;
    }
    if(x >= 5){
      return ret * 5;
    }else if(x >= 2){
      return ret * 2
    }else {
      return ret;
    }
  }

  function find_max(data){
    var max = 0;
    for(var i = 0; i < data.length;i++){
      if(data[i].total > max){
        max = data[i].total
      }
    }
    return max;
  }
};

exports.conf = {
  enabled: true,
  guildOnly: false,
  aliases: ['mrl'],
  permLevel: permLevel.INACTIVEMENTOR
};

exports.help = {
  name: 'mentor-raid-log',
  description: 'shows mentor-raid-log graph for current month',
  usage: '!mentor-raid-log'
};
