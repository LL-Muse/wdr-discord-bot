exports.run = async function(client, message){
  if (message.toString().trim().length != 8) {
    let modlog = client.channels.cache.get('417638357959573505');
    let modmailMessage = await client.channels.cache.get(modlog.id).send(`@here Modmail from ${message.author} - ${message.toString().substring(8)}`);
      message.author.send("Modmail has been sent.");

      const connection = client.connection;
    var sql_command = "INSERT INTO WDR.MODMAIL (message_id, user_modmailed) VALUES ('"+modmailMessage.id+"', '"+message.author.id+"')";
    connection.query(sql_command, function(error, result, fields){
      if(error) { throw error }
    })

  } else {
    message.author.send("This command sends a message to the mod channel, you must include reasoning for the message. For example: `!modmail I am having an issue with ...`")
  }
  message.delete();
  };
  
  exports.conf = {
    enabled: true,
    guildOnly: false,
    aliases: [],
    permLevel: permLevel.EVERYONE
  };
  
  exports.help = {
    name: "modmail",
    description: "send message to mods",
    usage: "modmail <message>"
  };
  