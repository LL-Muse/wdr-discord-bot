exports.run = function(client, message){

  // User must be in channel
  if (!message.member.voice.channel) {
    message.channel.send(`You must be in the voice channel from which you want to move users.`);
    return;
  }
  let start_vc = message.member.voice.channel;

  // Command must contain target channel ID
  let tokens = message.content.split(" ");
  if (tokens.length <= 1) {
    message.channel.send(`Must specify target channel (via Copy ID).`);
    return;
  }
  let target_vc_id = tokens[1];

  // Target channel ID must be a voice channel
  let target_vc = message.guild.channels.cache.get(target_vc_id);
  if (!target_vc) {
    message.channel.send(`Couldn't find target channel.`);
    return;
  }
  if (target_vc.type != "voice") {
    message.channel.send(`Target channel ID must be for a must be a voice channel.`);
    return;
  }

  // Target channel must be available to the mover
  if (!target_vc.permissionsFor(message.member).has("CONNECT")) {
    message.channel.send(`You must be able to enter the target channel.`);
    return;
  }


  let members = start_vc.members.array();
  let movedlist = new Array();
  let moverperms = target_vc.permissionsFor(message.member).toArray();

  for (let i = 0; i < members.length; i++) {
    movedlist.push(members[i].displayName);
    if (!target_vc.permissionsFor(members[i]).has("CONNECT")) {
      target_vc.updateOverwrite(members[i],{ 'CONNECT': true }).then(()=>{
        members[i].voice.setChannel(target_vc);
      }).then(()=>{
        setTimeout(()=>{
          target_vc.permissionOverwrites.get(members[i].id).delete();
        }, 1000)
      })
    } else {
      members[i].voice.setChannel(target_vc);
    }
  }

  // Log move.
  message.channel.send(`**${message.member.displayName}** has moved the users in **${start_vc.name}** to **${target_vc.name}**.\n\nMoved: ${movedlist.join(", ")}`);
};

exports.conf = {
  enabled: true,
  guildOnly: false,
  aliases: ["mm"],
  permLevel: permLevel.INACTIVEMENTOR
};

exports.help = {
  name: "massmove",
  description: "Move all from current voice channel into another",
  usage: "massmove <target channel ID>"
};