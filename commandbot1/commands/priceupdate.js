exports.run = async function(client, message, args){

	const fetch = require("node-fetch");

	const price_urls = {
		rune_pickaxe : "http://services.runescape.com/m=itemdb_oldschool/Runescape/viewitem?obj=1275",
		twisted_bow : "http://services.runescape.com/m=itemdb_oldschool/Twisted_bow/viewitem?obj=20997",
		helm_of_neitiznot : "http://services.runescape.com/m=itemdb_oldschool/Helm_of_neitiznot/viewitem?obj=10828",
		amulet_of_fury : "http://services.runescape.com/m=itemdb_oldschool/Amulet_of_fury/viewitem?obj=6585",
		abyssal_whip : "http://services.runescape.com/m=itemdb_oldschool/Runescape/viewitem?obj=4151",
		kraken_tentacle : "http://services.runescape.com/m=itemdb_oldschool/Kraken_tentacle/viewitem?obj=12004",
		zamorak_chaps : "http://services.runescape.com/m=itemdb_oldschool/Zamorak_chaps/viewitem?obj=10372",
		dragon_boots : "http://services.runescape.com/m=itemdb_oldschool/Runescape/viewitem?obj=11840", 
		berserker_ring : "http://services.runescape.com/m=itemdb_oldschool/Berserker_ring/viewitem?obj=6737",
		ahrims_robetop : "http://services.runescape.com/m=itemdb_oldschool/Ahrim's_robetop/viewitem?obj=4712",
		ahrims_robeskirt : "http://services.runescape.com/m=itemdb_oldschool/Ahrim's_robeskirt/viewitem?obj=4714",
		zamorak_dhide : "http://services.runescape.com/m=itemdb_oldschool/Zamorak_d'hide/viewitem?obj=10370",
		zamorak_dhide_boots : "http://services.runescape.com/m=itemdb_oldschool/Zamorak_d'hide_boots/viewitem?obj=19936",
		occult_necklace : "http://services.runescape.com/m=itemdb_oldschool/Occult_necklace/viewitem?obj=12002",
		toxic_blowpipe : "http://services.runescape.com/m=itemdb_oldschool/Toxic_blowpipe_(empty)/viewitem?obj=12924",
		toxic_trident : "http://services.runescape.com/m=itemdb_oldschool/Uncharged_toxic_trident/viewitem?obj=12900",
		bandos_godsword : "http://services.runescape.com/m=itemdb_oldschool/Bandos_godsword/viewitem?obj=11804",
		dragon_pickaxe : "http://services.runescape.com/m=itemdb_oldschool/Dragon_pickaxe/viewitem?obj=11920",
		necklace_of_anguish : "http://services.runescape.com/m=itemdb_oldschool/Necklace_of_anguish/viewitem?obj=19547",
		rigour : "http://services.runescape.com/m=itemdb_oldschool/Dexterous_prayer_scroll/viewitem?obj=21034",
		tormented_bracelet : "http://services.runescape.com/m=itemdb_oldschool/Tormented_bracelet/viewitem?obj=19544",
		amulet_of_torture : "http://services.runescape.com/m=itemdb_oldschool/Amulet_of_torture/viewitem?obj=19553",
		dragon_hunter_lance : "http://services.runescape.com/m=itemdb_oldschool/Dragon_hunter_lance/viewitem?obj=22978",
		dragon_warhammer : "http://services.runescape.com/m=itemdb_oldschool/Dragon_warhammer/viewitem?obj=13576", 
		scythe_of_vitur : "http://services.runescape.com/m=itemdb_oldschool/Scythe_of_vitur_(uncharged)/viewitem?obj=22486",
		dragon_crossbow : "http://services.runescape.com/m=itemdb_oldschool/Dragon_crossbow/viewitem?obj=21902",
		armadyl_crossbow : "http://services.runescape.com/m=itemdb_oldschool/Armadyl_crossbow/viewitem?obj=11785", 
		dragon_hunter_crossbow : "http://services.runescape.com/m=itemdb_oldschool/Dragon_hunter_crossbow/viewitem?obj=21012",
		sanguinesti_staff : "http://services.runescape.com/m=itemdb_oldschool/Sanguinesti_staff_(uncharged)/viewitem?obj=22481",
		ferocious_gloves : "http://services.runescape.com/m=itemdb_oldschool/Hydra_leather/viewitem?obj=22983",
		serpentine_helm : "http://services.runescape.com/m=itemdb_oldschool/Serpentine_helm_(uncharged)/viewitem?obj=12929",
		primordial_boots : "http://services.runescape.com/m=itemdb_oldschool/Primordial_boots/viewitem?obj=13239",
		augury : "http://services.runescape.com/m=itemdb_oldschool/Arcane_prayer_scroll/viewitem?obj=21079",
		ancestral_hat : "http://services.runescape.com/m=itemdb_oldschool/Ancestral_hat/viewitem?obj=21018", 
		ancestral_robe_bottom : "http://services.runescape.com/m=itemdb_oldschool/Ancestral_robe_bottom/viewitem?obj=21024",
		ancestral_robe_top : "http://services.runescape.com/m=itemdb_oldschool/Ancestral_robe_top/viewitem?obj=21021",
		armadyl_chainskirt : "http://services.runescape.com/m=itemdb_oldschool/Armadyl_chainskirt/viewitem?obj=11830", 
		armadyl_chestplate : "http://services.runescape.com/m=itemdb_oldschool/Armadyl_chestplate/viewitem?obj=11828",
		armadyl_helmet : "http://services.runescape.com/m=itemdb_oldschool/Armadyl_helmet/viewitem?obj=11826",
		pegasian_boots : "http://services.runescape.com/m=itemdb_oldschool/Pegasian_boots/viewitem?obj=13237",
		avernic_defender : "http://services.runescape.com/m=itemdb_oldschool/Avernic_defender_hilt/viewitem?obj=22477",
		arcane_spirit_shield : "http://services.runescape.com/m=itemdb_oldschool/Arcane_spirit_shield/viewitem?obj=12825",
		ghrazi_rapier : "http://services.runescape.com/m=itemdb_oldschool/Ghrazi_rapier/viewitem?obj=22324",
		abyssal_bludgeon : "http://services.runescape.com/m=itemdb_oldschool/Abyssal_bludgeon/viewitem?obj=13263",
		bandos_tassets : "http://services.runescape.com/m=itemdb_oldschool/Bandos_tassets/viewitem?obj=11834",
		bandos_chestplate : "http://services.runescape.com/m=itemdb_oldschool/Bandos_chestplate/viewitem?obj=11832",
		rune_crossbow : "http://services.runescape.com/m=itemdb_oldschool/Runescape/viewitem?obj=9185",
		dragon_sword : "http://services.runescape.com/m=itemdb_oldschool/Dragon_sword/viewitem?obj=21009",
		abyssal_dagger : "http://services.runescape.com/m=itemdb_oldschool/Runescape/viewitem?obj=13265",
		basilisk_jaw : "http://services.runescape.com/m=itemdb_oldschool/Basilisk_jaw/viewitem?obj=24268", 
		mages_book : "http://services.runescape.com/m=itemdb_oldschool/Runescape/viewitem?obj=6889",
		dragon_claws : "http://services.runescape.com/m=itemdb_oldschool/Dragon+claws/viewitem?obj=13652"
	}

	const connection = client.connection;

	console.log("updating prices");

	var count = 0;

	for (var key in price_urls) {
		var resp = await fetch(price_urls[key]);
		var text = await resp.text();

		text = text.substring(text.indexOf("Current Guide Price"), text.length);
		text = text.substring(text.indexOf("\'")+1, text.length);
		text = text.substring(0, text.indexOf("\'"))
		text = text.replace(/,/g, '');

		if(text != "no-js lt-ie10 lt-ie9 lt-ie8 lt-ie7"){
			var sql_command_1 = "DELETE FROM WDR.GEAR_PRICE WHERE item =\'" + key + "\'";
			if(args[0] == "-debug"){
				message.channel.send(sql_command_1);
			}
			connection.query(sql_command_1, async function(error, result, fields){
				if(error){ throw error }
			});

			var sql_command_2 = "INSERT INTO WDR.GEAR_PRICE (item, price) VALUES (\"" + key + "\", " + text + ");";
			if(args[0] == "-debug"){
				message.channel.send(sql_command_2);
			}
			connection.query(sql_command_2, async function(error, result, fields){
				if(error){ throw error }
			});
		}
	}
	message.channel.send("done");
};

exports.conf = {
  enabled: true,
  guildOnly: false,
  aliases: [],
  permLevel: permLevel.MOD
};

exports.help = {
  name: 'price-update',
  description: 'fetchs the most recent gear prices',
  usage: '!price-update'
};
