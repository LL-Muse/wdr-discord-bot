exports.run = function(client, message, args) {

  //offences channel
  let offences = message.guild.channels.cache.find(channel => channel.id == "408119551566282752");

  //putting arguments in a different array
  let argsString = args.join(" ");
  let argsSeperatedCorrectly = argsString.split("|");

  //checking argument count
  if(argsSeperatedCorrectly.length < 4){
    message.channel.send("Not enough arguments provided")
    return;
  }
  if(argsSeperatedCorrectly.length > 4){
    message.channel.send("Too many arguments provided")
    return;
  }

  //these will always be the same order
  let userId =  argsSeperatedCorrectly[0].trim();
  let offence = argsSeperatedCorrectly[1].trim();
  let punishment = argsSeperatedCorrectly[2].trim();
  let totalStrikes = argsSeperatedCorrectly[3].trim();

  //making message here
  let msg = "**Discord Name:** <@!"+userId+">\n";//single line break
  msg += "**Discord ID:** "+userId+"\n\n";//Double line break here
  msg += "**Offence:** "+offence+"\n\n"; //Another double line break required here
  msg += "**Punishment:** "+punishment+"\n"; //Single line break
  msg += "**Total Strikes:** "+totalStrikes+"\n"; //One more line break
  msg += "**Issued by:** <@!"+message.member.id+">\n";
  msg += "━━━━━━━━━━━━━━━━━━";

  offences.send(msg);
}
exports.conf = {
  enabled: true,
  guildOnly: false,
  aliases: [],
  permLevel: permLevel.TRIALMOD
};

exports.help = {
  name: 'offence',
  description: '',
  usage: 'offence userid | offence | punishment | total strikes'
};