exports.run = async function(client, message, args){

    const fetch = require("node-fetch");

    const skills = ['overall','attack','defence','strength','hitpoints','ranged','prayer','magic','cooking','woodcutting','fletching','fishing','firemaking','crafting','smithing','mining','herblore','agility','thieving','slayer','farming','runecraft','hunter','construction'];

    const connection = client.connection;

    var name = args.join(" ");
    if(name == undefined || name == null || name == ""){
        message.channel.send("must provide rsn");
        return;
    }

    var lookup = await getStats(name);
    if(lookup.response == 200){
        var sql_command_4 = "DELETE FROM WDR.USER_STATS WHERE rsn=\'" + name + "\'";
        await connection.query(sql_command_4, function(error, result, fields){
            if(error){ throw error }
        })
        var sql_command_5 = "INSERT INTO WDR.USER_STATS (rsn, stats, last_update) VALUES (\'" + name + "\',\'" + JSON.stringify(lookup.data) + "\',\'" + new Date().toISOString().slice(0, 19).replace('T', ' ') + "\')";
        await connection.query(sql_command_5, function(error, result, fields){
            if(error){ throw error }
        })

        message.channel.send("data saved");
        message.channel.send(format_stats(lookup.data));
    }else{
        message.channel.send("rsn not found on hiscore. Response code: "+lookup.response);
    }


	function format_stats (data){
		var ret = "```";
		ret = ret + "╔" + repeatchar("═", 14) + "╦" + repeatchar("═", 7) + "╦" + repeatchar("═", 12) + "╗\n";
		ret = ret + "║ " + center_fill_with_spaces("skill", 12) + " ║ " + center_fill_with_spaces("level", 5) + " ║ " + center_fill_with_spaces("exp", 10) + " ║\n";
		ret = ret + "╠" + repeatchar("═", 14) + "╬" + repeatchar("═", 7) + "╬" + repeatchar("═", 12) + "╣\n";
		for(var i = 0; i < skills.length; i++){
			ret = ret + "║ " + left_fill_with_spaces(skills[i], 12) + " ║ " + right_fill_with_spaces(data[skills[i]].level, 5) + " ║ " + right_fill_with_spaces(data[skills[i]].exp, 10) + " ║\n";
		}	
		ret = ret + "╚" + repeatchar("═", 14) + "╩" + repeatchar("═", 7) + "╩" + repeatchar("═", 12) + "╝";
		ret = ret + "```";	
		return ret;
	}

	function right_fill_with_spaces (mes, length){
		var ret = mes;
		while(ret.length < length){
			ret = " " + ret;
		}
		return ret;
	}

	function left_fill_with_spaces (mes, length){
		var ret = mes;
		while(ret.length < length){
			ret = ret + " ";
		}
		return ret;
	}

	function center_fill_with_spaces (mes, length){
		var ret = mes;
		while(ret.length < length - 1){
			ret = " " + ret + " ";
		}
		if(ret.length < length){
			ret = ret + " ";
		}
		return ret;
	}

	function repeatchar(char, amount){
		var ret = "";
		while(ret.length < amount){
		ret += char;
		}
		return ret;
	}
    async function getStats(rsn) {
        const response = await fetch("https://secure.runescape.com/m=hiscore_oldschool/a=97/index_lite.ws?player=" + encodeURIComponent(rsn), {"redirect": "manual"});
        const text = await response.text();
        if(response.status == 200){
            return ({"response":response.status, "data": parseStatsData(text)})
        }else{
            return({"response":response.status, "data":null});
        }
    }

    function parseStatsData (data) {
        const skills = ['overall','attack','defence','strength','hitpoints','ranged','prayer','magic','cooking','woodcutting','fletching','fishing','firemaking','crafting','smithing','mining','herblore','agility','thieving','slayer','farming','runecraft','hunter','construction'];
        var sortedData = {};
        var splitData = data.split('\n');
        for (var i = 0; i < skills.length; i++) {
            var level = splitData[i].split(',')[1];
            var exp = splitData[i].split(',')[2];
            sortedData[skills[i]] = {"level" : level, "exp" : exp};
        }
        return sortedData;
    }

};

exports.conf = {
  enabled: true,
  guildOnly: false,
  aliases: [],
  permLevel: permLevel.INACTIVEMENTOR
};

exports.help = {
  name: 'save-stats',
  description: 'staff only',
  usage: 'staff only'
};

