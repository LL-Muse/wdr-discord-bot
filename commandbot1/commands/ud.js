exports.run = async function(client, message, args){

    let users = message.mentions.members;
    var user_ids = Array.from(users.keys());
    if(user_ids.length === 0){
        message.channel.send("Please ping at least 1 user to have a UD with");
        return;
    }
    let perms = [
        {//mod
            id: "408106863117336577",
            allow: ['MANAGE_CHANNELS', 'MANAGE_ROLES', 'VIEW_CHANNEL', 'SEND_MESSAGES']
        },
        {//trial mod
            id : "423653735449886720",
            allow : ['VIEW_CHANNEL', 'SEND_MESSAGES'],
            deny : ['MANAGE_ROLES']
        },
        {//everyone
            id : message.guild.roles.everyone,
            deny : ['VIEW_CHANNEL']
        },
        {//admin bots
            id : "482771653273714688",
            allow : ['VIEW_CHANNEL']
        },
        {//Twisty
            id : "228019028755611648",
            allow : ['VIEW_CHANNEL']
        }
        ];
    for(var i = 0; i < user_ids.length; i++){//mentions
        perms.push({id: user_ids[i], allow: ['VIEW_CHANNEL', 'SEND_MESSAGES']}) 

    }     
    await message.guild.channels.create(args[0].replace(/ /g, "-") , {type: 'text', parent:'481378650877329417', permissionOverwrites:perms});
 
};

exports.conf = {
  enabled: true,
  guildOnly: false,
  aliases: [],
  permLevel: permLevel.TRIALMOD
};

exports.help = {
  name: 'ud',
  description: '',
  usage: 'ud channel_name @user1 @user2 ...'
};
