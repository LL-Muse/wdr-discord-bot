exports.run = async function(client, message){
const {google} = require("googleapis"); //npm install googleapis@39 --save
    const google_sheet_keys = require("/root/wdrbot/google_sheet_keys.json"); //top secret do not share, file path may need to be adjusted as well
    const Discord = require("discord.js");

    var Banlist_Column_Headers = [];

  const google_client = new google.auth.JWT(
      google_sheet_keys.client_email,
      null,
      google_sheet_keys.private_key,
      ["https://www.googleapis.com/auth/spreadsheets"] //read write permissions
    );

    google_client.authorize(function(error, tokens){
      if(error){
        console.log(error);
        return;
      }
    });

        const google_sheet = google.sheets({version:"v4", auth:google_client})
  const opt= {
    spreadsheetId: "1RCSAGdCyef2h2whTga6Q5uCAyEh3ffU9WOKmaryl2gs",
    range: "Data"
  }
  var data = await google_sheet.spreadsheets.values.get(opt);
  var banlist = data.data.values;
    var names = new Set();

  for(var i = 1; i < banlist.length; i++){
    if(banlist[i][8] == undefined){
      continue;
    }
    if(banlist[i][8].startsWith("Repaid")) {
      continue;
    }
    names.add(banlist[i][0])
    if(banlist[i][2] != undefined && banlist[i][2] != ""){
      var alt_names = banlist[i][2].split(",").map(function (x){ return x.trim()});
      for(var j = 0; j < alt_names.length; j++){
        names.add(alt_names[j]);
      }
    }
  }

    var members = message.guild.members.cache;
    for(var member of members){
      var user = await message.guild.members.fetch(member[1].user.id);
      var name = user.nickname;
      if (name == null || name == undefined) {//use discord name if no sever nickname
          name = member[1].user.username;
      }

      if(user.roles.cache.has("408221074833145863"))
        continue;

      if(names.has(name)){
        message.channel.send("<@!" + member[1].user.id + ">");
      }
    }
	message.channel.send("done");


};

exports.conf = {
 enabled: true,
 guildOnly: false,
 aliases: [],
 permLevel: permLevel.TRIALMOD
};

exports.help = {
 name: 'sus',
 description: 'sus',
 usage: 'sus'
};
