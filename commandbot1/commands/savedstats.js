exports.run = async function(client, message, args){

    const skills = ['overall','attack','defence','strength','hitpoints','ranged','prayer','magic','cooking','woodcutting','fletching','fishing','firemaking','crafting','smithing','mining','herblore','agility','thieving','slayer','farming','runecraft','hunter','construction'];

    const connection = client.connection;

	var name = args.join(" ")

    if(name == undefined || name == null || name == ""){
        message.channel.send("must provide rsn");
        return;
    }

    var sql_command = "SELECT * FROM WDR.USER_STATS WHERE rsn=\'" + name + "\'";
    connection.query(sql_command, function(error, result, fields){
        if(error) { throw error }
        if(result == undefined || result == null || result.length == 0){
            message.channel.send(name + " not found"); 
            return;
        }else{
            message.channel.send(result[0].last_update + " ");
			message.channel.send(format_stats(JSON.parse(result[0].stats)));
        }
    })

	function format_stats (data){
		var ret = "```";
		ret = ret + "╔" + repeatchar("═", 14) + "╦" + repeatchar("═", 7) + "╦" + repeatchar("═", 12) + "╗\n";
		ret = ret + "║ " + center_fill_with_spaces("skill", 12) + " ║ " + center_fill_with_spaces("level", 5) + " ║ " + center_fill_with_spaces("exp", 10) + " ║\n";
		ret = ret + "╠" + repeatchar("═", 14) + "╬" + repeatchar("═", 7) + "╬" + repeatchar("═", 12) + "╣\n";
		for(var i = 0; i < skills.length; i++){
			ret = ret + "║ " + left_fill_with_spaces(skills[i], 12) + " ║ " + right_fill_with_spaces(data[skills[i]].level, 5) + " ║ " + right_fill_with_spaces(data[skills[i]].exp, 10) + " ║\n";
		}	
		ret = ret + "╚" + repeatchar("═", 14) + "╩" + repeatchar("═", 7) + "╩" + repeatchar("═", 12) + "╝";
		ret = ret + "```";	
		return ret;
	}

	function right_fill_with_spaces (mes, length){
		var ret = mes;
		while(ret.length < length){
			ret = " " + ret;
		}
		return ret;
	}

	function left_fill_with_spaces (mes, length){
		var ret = mes;
		while(ret.length < length){
			ret = ret + " ";
		}
		return ret;
	}

	function center_fill_with_spaces (mes, length){
		var ret = mes;
		while(ret.length < length - 1){
			ret = " " + ret + " ";
		}
		if(ret.length < length){
			ret = ret + " ";
		}
		return ret;
	}

	function repeatchar(char, amount){
		var ret = "";
		while(ret.length < amount){
		ret += char;
		}
		return ret;
	}

};

exports.conf = {
  enabled: true,
  guildOnly: false,
  aliases: [],
  permLevel: permLevel.TRIALMOD
};

exports.help = {
  name: 'saved-stats',
  description: 'staff only',
  usage: 'staff only'
};

