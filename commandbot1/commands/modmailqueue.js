exports.run = async function(client, message) {
  const connection = client.connection;

  let sql_command = "SELECT message_id FROM WDR.MODMAIL WHERE user_reacted IS NULL ORDER BY modmail_time ASC";
  var data = "";
  connection.query(sql_command, function(error, result, fields){
    if(error) { throw error }
    if(result != undefined && result != null){
      data = result;
      if(data.length == 0){
        message.channel.send("No modmails in queue");
        return;
      }
      msg = "";
      for(var i = 0; i < data.length; i++) {
        if(i === 10){
          break;
        }
        msg += "<https://discordapp.com/channels/408082929940430858/417638357959573505/"+data[i].message_id+">\n"
      }
      if(data.length > 10){
        msg+= (data.length - 10)+" more results";
      }

      message.channel.send(msg);

    }
  })



}
exports.conf = {
  enabled: true,
  guildOnly: false,
  aliases: ["mmq"],
  permLevel: permLevel.TRIALMOD
};

exports.help = {
  name: "modmailqueue",
  description: "Shows modmails not answered yet",
  usage: "modmailqueue"
};
