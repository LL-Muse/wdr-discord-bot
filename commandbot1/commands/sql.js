exports.run = async function(client, message, args){

	const fs = require('fs');


	const connection = client.connection;


	var sql_command_1 = args.join(" ");
	console.log(sql_command_1);
	if(sql_command_1.toLowerCase().startsWith("delete") || sql_command_1.toLowerCase().startsWith("drop")){
		message.channel.send("You are not allowed to do that.");
		return;
	}
	connection.query(sql_command_1, async function(error, result, fields){
		if(error){ 
			message.channel.send("```json\n"+JSON.stringify(error)+"\n```");
			throw error 
		}
		if(result != undefined && result != null){
			fs.writeFile('sql_output.txt', replaceAll(JSON.stringify(result), "}", "}\n") , (err) => { 
    			if (err) { throw err }  
			})
	
		message.channel.send("", { files: ["./sql_output.txt"] }); 

		}
	});

	function escapeRegExp(string){
    	return string.replace(/[.*+?^${}()|[\]\\]/g, "\\$&");
	}

	function replaceAll(str, term, replacement) {
		return str.replace(new RegExp(escapeRegExp(term), 'g'), replacement);
	}
};

exports.conf = {
  enabled: true,
  guildOnly: false,
  aliases: [],
  permLevel: permLevel.MOD
};

exports.help = {
  name: 'sql',
  description: 'executes sql querry on database',
  usage: 'sql'
};
