exports.run = function(client, message){
   if (message.member.voiceChannel) {
    message.channel.send(`<https://discordapp.com/channels/${message.guild.id}/${message.member.voiceChannelID}/>`);
  } else {
    message.channel.send(`You must be in a voice channel to start screen sharing.`);
  }
};

exports.conf = {
  enabled: true,
  guildOnly: false,
  aliases: ["ss"],
  permLevel: permLevel.INACTIVEMENTOR
};

exports.help = {
  name: "screenshare",
  description: "Screenshare link for your voice channel",
  usage: "screenshare"
};
