exports.run = async function(client, message, args){

    const fetch = require("node-fetch");

    var name = args.join(" ");
    if(name == undefined || name == null || name == ""){
        message.channel.send("must provide rsn");
        return;
    }

    var lookup = await getStats(name);
    if(lookup.real){
		format_kc(lookup.data, message)
    }else{
        message.channel.send("rsn not found on hiscore");
    }

function format_kc (data, message){
        var ret = "```";
        const offset_1 = 17;
        const offset_2 = 6;
        const offset_3 = 3;
        const offset_4 = 8;
        const bosses = ["Abyssal Sire", "Alchemical Hydra", "Callisto", "Cerberus", "Chambers of Xeric", "CoX CM", "Chaos Elemental", "Chaos Fanatic", "Commander Zilyana", "Corporeal Beast", "Dagannoth Prime", "Dagannoth Rex", "Dagannoth Supreme", "General Graardor", "Giant Mole", "Dusk & Dawn", "Kalphite Queen", "King Black Dragon", "Kraken", "Kree'Arra", "K'ril Tsutsaroth", "Nightmare", "Sarachnis", "Scorpia", "Gauntlet", "Corrupt Gauntlet", "Theatre of Blood", "Thermy", "TzKal-Zuk", "TzTok-Jad", "Venenatis", "Vet'ion", "Vorkath", "Zulrah"];
        ret = ret + "╔" + repeatchar("═", offset_1 + 2) + "╦" + repeatchar("═", offset_2) + "╦" + repeatchar("═", offset_3) + "╦" + repeatchar("═", offset_4) + "╗\n";
        ret = ret + "║ " + center_fill_with_spaces("boss", offset_1) + " ║" + center_fill_with_spaces("kills", offset_2) + "║" + center_fill_with_spaces("kph", offset_3) + "║" + center_fill_with_spaces("ehb", offset_4) + "║\n";
        ret = ret + "╠" + repeatchar("═", offset_1 + 2) + "╬" + repeatchar("═", offset_2) + "╬" + repeatchar("═", offset_3) + "╬" + repeatchar("═", offset_4) + "╣\n";
        for(var i = 0; i < bosses.length; i++){
            ret = ret + "║ " + left_fill_with_spaces(bosses[i], offset_1) + " ║" + right_fill_with_spaces(data[bosses[i]].kc, offset_2) + "║" + right_fill_with_spaces(data[bosses[i]].kph + "", offset_3) + "║" + right_fill_with_spaces(data[bosses[i]].ehb + "", offset_4) + "║\n";
        }
        ret = ret + "╠" + repeatchar("═", offset_1 + 2) + "╬" + repeatchar("═", offset_2) + "╬" + repeatchar("═", offset_3) + "╬" + repeatchar("═", offset_4) + "╣\n"   
        ret = ret + "║ " + left_fill_with_spaces("Total", offset_1) + " ║" + right_fill_with_spaces(" ", offset_2) + "║" + right_fill_with_spaces(" ", offset_3) + "║" + right_fill_with_spaces(data["Total"].ehb + "", offset_4) + "║\n";
        ret = ret + "╚" + repeatchar("═", offset_1 + 2) + "╩" + repeatchar("═", offset_2) + "╩" + repeatchar("═", offset_3) + "╩" + repeatchar("═", offset_4) + "╝";

        ret = ret + "```";  
        console.log(ret.length)
        message.channel.send(ret);
    }

    function right_fill_with_spaces (mes, length){
        var ret = mes;
        while(ret.length < length){
            ret = " " + ret;
        }
        return ret;
    }

    function left_fill_with_spaces (mes, length){
        var ret = mes;
        while(ret.length < length){
            ret = ret + " ";
        }
        return ret;
    }

    function center_fill_with_spaces (mes, length){
        var ret = mes;
        while(ret.length < length - 1){
            ret = " " + ret + " ";
        }
        if(ret.length < length){
            ret = ret + " ";
        }
        return ret;
    }

    function repeatchar(char, amount){
        var ret = "";
        while(ret.length < amount){
        ret += char;
        }
        return ret;
    }
    async function getStats(rsn) {
        const response = await fetch("http://services.runescape.com/m=hiscore_oldschool/index_lite.ws?player=" + encodeURIComponent(rsn));
        const text = await response.text();
        if(!text.includes("<title>404 - Page not found</title>")){
            return ({"real":true, "data": parseKCData(text)})
        }else{
            return({"real":false, "data":null});
        }
    }

    function parseKCData (data) {
        const bosses = ["Abyssal Sire", "Alchemical Hydra", "Barrows Chests", "Bryophyta", "Callisto", "Cerberus", "Chambers of Xeric", "CoX CM", "Chaos Elemental", "Chaos Fanatic", "Commander Zilyana", "Corporeal Beast", "Crazy Arch", "Dagannoth Prime", "Dagannoth Rex", "Dagannoth Supreme", "Deranged Arch", "General Graardor", "Giant Mole" , "Dusk & Dawn", "Hespori", "Kalphite Queen", "King Black Dragon", "Kraken", "Kree'Arra", "K'ril Tsutsaroth", "Mimic", "Nightmare", "Obor", "Sarachnis", "Scorpia", "Skotizo", "Gauntlet", "Corrupt Gauntlet", "Theatre of Blood", "Thermy", "TzKal-Zuk", "TzTok-Jad", "Venenatis", "Vet'ion", "Vorkath", "Wintertodt", "Zalcano", "Zulrah"];
        const kph = {"Abyssal Sire" : 30, "Alchemical Hydra" : 27, "Callisto" : 30, "Cerberus" : 61, "Chambers of Xeric" : 3.5, "CoX CM" : 1.8, "Chaos Elemental" : 48, "Chaos Fanatic" : 100, "Commander Zilyana" : 25, "Corporeal Beast" : 14, "Dagannoth Prime" : 96, "Dagannoth Rex" : 96, "Dagannoth Supreme" : 96, "General Graardor" : 30, "Giant Mole" : 90 , "Dusk & Dawn" : 30, "Kalphite Queen" : 35, "King Black Dragon" : 100, "Kraken" : 80, "Kree'Arra" : 28, "K'ril Tsutsaroth" : 36, "Nightmare" : 10, "Sarachnis" : 45, "Scorpia" : 100, "Gauntlet" : 10, "Corrupt Gauntlet" : 7.5, "Theatre of Blood" : 3, "Thermy" : 110, "TzKal-Zuk" : 0.8, "TzTok-Jad" : 2, "Venenatis" : 44, "Vet'ion" : 30, "Vorkath" : 32, "Zulrah" : 35};

        const offset = 35;
        var sortedData = {};
        var splitData = data.split('\n');
        var total_ehb = 0;

        for (var i = offset; i - offset < bosses.length; i++) {
            var kc = splitData[i].split(',')[1];
            if(kc == "-1"){ kc = "0"}

            

            if(kph[bosses[i - offset]] != undefined){
                var ehb = (parseFloat(kc)/kph[bosses[i - offset]]).toFixed(2)
                sortedData[bosses[i - offset]] = {"kc" : kc, "kph" : kph[bosses[i - offset]], "ehb" : ehb}; 
                total_ehb += parseFloat(ehb);
            }

            
        }
        sortedData["Total"] = {"kc" : "", "kph" : "", "ehb" : total_ehb.toFixed(2)}
        return sortedData;
    }
};

exports.conf = {
  enabled: true,
  guildOnly: false,
  aliases: [],
  permLevel: permLevel.EVERYONE
};

exports.help = {
  name: 'ehb',
  description: 'efficent hours bossed (rates from oblv discord)',
  usage: 'ehb rsn'
};

