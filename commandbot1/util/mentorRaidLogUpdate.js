

module.exports = async function(message, client) {
  const connection = client.connection;

  if (message.content.toLowerCase() != "cox" && message.content.toLowerCase() != "tob" && message.content.toLowerCase() != "cm" && message.content.toLowerCase() != "adv") {
    message.author.send("Please only type cox, cm, adv(cox) or tob");
    message.delete();
    return;
  }
  var name = message.guild.member(message.author.id).nickname;
  if (name == null || name == undefined) {//use discord name if no sever nickname
    name = message.guild.member(message.author.id).user.username;
  }

  var sql_command_4 = "DELETE FROM WDR.MENTOR_IDS WHERE user_id=\'" + message.author.id + "\'";
  await connection.query(sql_command_4, function (error, result, fields) {
    if (error) {
      throw error
    }
  })
  var sql_command_5 = "INSERT INTO WDR.MENTOR_IDS (user_id, user_discord_name) VALUES (\'" + message.author.id + "\',\'" + name + "\')";
  await connection.query(sql_command_5, function (error, result, fields) {
    if (error) {
      throw error
    }
  })

  var sql_command = "INSERT INTO WDR.MENTOR_RAID_LOGS (user_id, user_discord_name, message, msg_time) VALUES (\'" + message.author.id + "\',\'" + name + "\',\'" + message.content.toLowerCase() + "\',\'" + new Date().toISOString().slice(0, 19).replace('T', ' ') + "\')";
  connection.query(sql_command, function (error, result, fields) {
    if (error) {
      throw error
    }
    message.delete();
  })
}