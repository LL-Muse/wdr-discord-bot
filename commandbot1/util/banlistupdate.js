var lastUpdate = new Date(0);
module.exports = async function(client){
  const {google} = require("googleapis"); //npm install googleapis@39 --save
  const google_sheet_keys = require("/root/wdrbot/google_sheet_keys.json"); //top secret do not share, file path may need to be adjusted as well

  const banupdates = "408119009427587072";
  const google_client = new google.auth.JWT(
    google_sheet_keys.client_email,
    null,
    google_sheet_keys.private_key,
    ["https://www.googleapis.com/auth/spreadsheets"] //read write permissions
  );

  const google_sheet = google.sheets({version:"v4", auth:google_client})
  const opt= {
    spreadsheetId: "1RCSAGdCyef2h2whTga6Q5uCAyEh3ffU9WOKmaryl2gs",
    range: "Data"
  }

  var data = await google_sheet.spreadsheets.values.get(opt);
  var banlist = data.data.values;

  var arr = [];
  var arr2 = [];

  arr.push(""); //unchecked box is empty string
  arr2.push(arr);

  for(var i = 0; i < banlist.length; i++){
    if(banlist[i][6] == 'TRUE'){//new entry
      if(!banlist[i][8].startsWith("Repaid")) {
        client.channels.cache.get(banupdates).send('Add this player to your ignore list:\n<:cox:651156619824070687> \`\`' + banlist[i][0] + "\`\` - " + banlist[i][1]);
      }
      const opt= {
        spreadsheetId: "1RCSAGdCyef2h2whTga6Q5uCAyEh3ffU9WOKmaryl2gs",
        range: "data!G" + (i+1),
        valueInputOption: "USER_ENTERED",
        resource: {values: arr2}
      }
      var res = await google_sheet.spreadsheets.values.update(opt);
      banlist[i][6] = '';
    }else if(banlist[i][7] == 'TRUE'){//name update
      if(!banlist[i][8].startsWith("Repaid")) {
        var banUpdatesMessage = "";
        if (Date.now()-lastUpdate >= 1500000) { //1500000 ms = 5 minutes
          banUpdatesMessage += "Update on a player:\n";
        }
        banUpdatesMessage+= '<:cox:651156619824070687> \`\`' + banlist[i][2].split(",")[0] + "\`\` has changed their name to \`\`" + banlist[i][0] + "\`\`";

        client.channels.cache.get(banupdates).send(banUpdatesMessage);
        lastUpdate = Date.now();
      }
      const opt= {
        spreadsheetId: "1RCSAGdCyef2h2whTga6Q5uCAyEh3ffU9WOKmaryl2gs",
        range: "data!H" + (i+1),
        valueInputOption: "USER_ENTERED",
        resource: {values: arr2}
      }
      var res = await google_sheet.spreadsheets.values.update(opt);
      banlist[i][7] = '';
    }
  }
}
