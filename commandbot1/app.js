const Discord = require("discord.js");
const client = new Discord.Client({partials: ['USER', 'GUILD_MEMBER', 'MESSAGE', 'REACTION']});
//const token = require("./settings.json").token;
const settings = require("./settings.json");
const token = settings.token;
const fs = require("fs");
const moment = require("moment");
require("./util/eventLoader")(client);
mysql = require("mysql");
config = require("/root/wdrbot/mysql_config.json");


const log = message => {
  console.log(`[${moment().format("YYYY-MM-DD HH:mm:ss")}] ${message}`);
};



client.connection = mysql.createConnection({
  host : config.mysql_ip,
  user : config.mysql_username,
  password : config.mysql_password
});

client.connection.connect(function(error){
  if(error){
    console.log("connection failed 4head");
    throw error;
  }
});

client.commands = new Discord.Collection();
client.aliases = new Discord.Collection();
fs.readdir("./commandbot1/commands/", (err, files) => {
  if (err) console.error(err);
  log(`Loading a total of ${files.length} commands.`);
  require("./permLevel.js");
  files.forEach(f => {
    let props = require(`./commands/${f}`);
    //log(`Loading Command: ${props.help.name}.`);
    client.commands.set(props.help.name, props);
    props.conf.aliases.forEach(alias => {
      client.aliases.set(alias, props.help.name);
    });
  });
});

client.reload = command => {
  return new Promise((resolve, reject) => {
    try {
      delete require.cache[require.resolve(`./commands/${command}`)];
      let cmd = require(`./commands/${command}`);
      client.commands.delete(command);
      client.aliases.forEach((cmd, alias) => {
        if (cmd === command) client.aliases.delete(alias);
      });
      client.commands.set(command, cmd);
      cmd.conf.aliases.forEach(alias => {
        client.aliases.set(alias, cmd.help.name);
      });
      resolve();
    } catch (e){
      reject(e);
    }
  });
};

client.elevation = message => {
  /* This function should resolve to an ELEVATION level which
     is then sent to the command handler for verification*/
  //TODO add other role ids to settings.json, and the other roles here
  if (message.author.id === '239907878738198529') return permLevel.ADMIN; //Metix
  if (message.author.id === '160889156430462976') return permLevel.ADMIN; //Spellign
  if(message.guild.id != "408082929940430858") return permLevel.EVERYONE;

  let permlvl = permLevel.EVERYONE;
  let cox_app = message.guild.roles.cache.get("572481959083638815")
  let tob_app = message.guild.roles.cache.get("579053677445120015")
  if (cox_app && message.member.roles.cache.has(cox_app.id) || tob_app && message.member.roles.cache.has(tob_app.id)) permlvl = permLevel.TRIALMENTOR;
  let inactivementor_role = message.guild.roles.cache.get(settings.inactivementorroleid);
  if(inactivementor_role && message.member.roles.cache.has(inactivementor_role.id)) permlvl = permLevel.INACTIVEMENTOR;
  let mentor_role = message.guild.roles.cache.get(settings.mentorroleid);
  let retiredmentor_role = message.guild.roles.cache.get("676854481933172771")
  if(retiredmentor_role && message.member.roles.cache.has(retiredmentor_role.id)) permlvl = permLevel.INACTIVEMENTOR;
  if (mentor_role && message.member.roles.cache.has(mentor_role.id)) permlvl = permLevel.MENTOR;
  let trial_mod_role = message.guild.roles.cache.get("423653735449886720");
  if(trial_mod_role && message.member.roles.cache.has(trial_mod_role.id)) permlvl = permLevel.TRIALMOD;
  let mod_role = message.guild.roles.cache.get(settings.modroleid);
  if (mod_role && message.member.roles.cache.has(mod_role.id)) permlvl = permLevel.MOD;
  let bot_dev_role = message.guild.roles.cache.get(settings.botdevroleid);
  if (bot_dev_role && message.member.roles.cache.has(bot_dev_role.id)) permlvl = permLevel.BOTDEV;
  let admin_role = message.guild.roles.cache.get(settings.adminroleid);
  if (admin_role && message.member.roles.cache.has(admin_role.id)) permlvl = permLevel.ADMIN;
  if (message.author.id === message.client.user.id) permlvl = permLevel.ADMIN;
  return permlvl;
};


client.login(token);
