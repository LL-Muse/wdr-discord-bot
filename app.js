const child = require("child_process");

start("rankbot1/app.js");
start("commandbot1/app.js");
function start (nodefile) {
  let proc = child.spawn("node", [nodefile]);

  proc.stdout.on("data", function (data) {
    console.log(data.toString());
  });

  proc.stderr.on("data", function (data) {
    console.log(data.toString());
  });

  proc.on("exit", function () {
    setTimeout(function () { start(nodefile); }, 5000);
  });
}
