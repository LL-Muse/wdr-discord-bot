module.exports = function otherEmbed(message, rank, step) {
    const Discord = require("discord.js");
    const channel = message.member.guild.channels.cache.find(channel => channel.name === "logs");
    if (!channel) return;
    const embed = new Discord.MessageEmbed()
        .setAuthor(`${message.member.user.tag}`, `${message.member.user.displayAvatarURL()}`)
        .setDescription(`${message.member.user.tag} ${step} ${rank}`)
        .setFooter("Rank Change")
        .setTimestamp()
        .setColor(0xB3B300);
    channel.send({embed});
}