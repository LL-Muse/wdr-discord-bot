const config = require("../config.json");

module.exports = function removeCox(message) {
    let nonraider = message.guild.roles.cache.get(config.nonraider);
    let role0 = message.guild.roles.cache.get(config.cox0);
    let role1 = message.guild.roles.cache.get(config.cox1);
    let role2 = message.guild.roles.cache.get(config.cox2);
    let role3 = message.guild.roles.cache.get(config.cox3);
    let role4 = message.guild.roles.cache.get(config.cox4);
    let role5 = message.guild.roles.cache.get(config.cox5);

    message.member.roles.remove(nonraider).catch(console.error);
    message.member.roles.remove(role0).catch(console.error);
    message.member.roles.remove(role1).catch(console.error);
    message.member.roles.remove(role2).catch(console.error);
    message.member.roles.remove(role3).catch(console.error);
    message.member.roles.remove(role4).catch(console.error);
    message.member.roles.remove(role5).catch(console.error);
};