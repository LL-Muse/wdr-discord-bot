const dmDelete = require("../util/dmDelete");
const otherEmbed = require("../util/otherEmbed");
const removeCox = require("../util/removeCox");
const removeTob = require("../util/removeTob");
const config = require("../config.json");
exports.run = function(client, message, messageArg) {
    var nonraider = message.guild.roles.cache.get(config.nonraider);
    if (!message.member.roles.cache.has(nonraider.id)) {
        otherEmbed(message, "nonraider", "added");
        message.member.fetch();
        removeCox(message);
        removeTob(message);
        message.member.roles.add(nonraider).catch(console.error);
        dmDelete(message, "**nonraider** " + config.notifRoleAdd);
    } else {
        otherEmbed(message, "nonraider", "removed");
        message.member.roles.remove(nonraider).catch(console.error);
        dmDelete(message, "**nonraider** " + config.notifRoleRem);
    }
};
exports.conf = {
    enabled: true,
    guildOnly: false,
    aliases: [],
    permLevel: 0
};

exports.help = {
    name: "nonraider",
    description: "assigns the nonraider role",
    usage: "nonraider"
};