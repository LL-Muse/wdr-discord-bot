const dmDelete = require("../util/dmDelete");
const otherEmbed = require("../util/otherEmbed");
const config = require("../config.json");
exports.run = function(client, message, messageArg) {
    var ffa = message.guild.roles.cache.get(config.ffa);

    var tierRequirement = "Tier 2";
    if (!message.member.roles.cache.has(ffa.id)) {
        if (message.member.roles.cache.has(config.cox2) || message.member.roles.cache.has(config.cox3) || message.member.roles.cache.has(config.cox4) || message.member.roles.cache.has(config.cox5)) {
            otherEmbed(message, "FFA", "added");
            message.member.roles.add(ffa).catch(console.error);
            dmDelete(message, "**FFA** " + config.notifRoleAdd);
        } else {
            dmDelete(message, config.errKCLow + "**" + tierRequirement + "**");
        }
    } else {
        otherEmbed(message, "FFA", "removed");
        message.member.roles.remove(ffa).catch(console.error);
        dmDelete(message, "**FFA** " + config.notifRoleRem);
    }
};
exports.conf = {
    enabled: true,
    guildOnly: false,
    aliases: [],
    permLevel: 0
};

exports.help = {
    name: "ffa",
    description: "assigns the ffa role",
    usage: "ffa"
};