const dmDelete = require("../util/dmDelete");
const otherEmbed = require("../util/otherEmbed");
const config = require("../config.json");
exports.run = function(client, message, messageArg) {
    var advmass = message.guild.roles.cache.get(config.advmass);
    var tierRequirement = "Tier 4";
    if (!message.member.roles.cache.has(advmass.id)) {
        if (message.member.roles.cache.has(config.cox5) || message.member.roles.cache.has(config.cox4)) {
            otherEmbed(message, "AdvMass", "added");
            message.member.roles.add(advmass).catch(console.error);
            dmDelete(message, "**AdvMass** " + config.notifRoleAdd);
        } else {
            dmDelete(message, config.errKCLow + "**" + tierRequirement + "**");
        }
    } else {
        otherEmbed(message, "AdvMass", "removed");
        message.member.roles.remove(advmass).catch(console.error);
        dmDelete(message, "**AdvMass** " + config.notifRoleRem);
    }
};
exports.conf = {
    enabled: true,
    guildOnly: false,
    aliases: [],
    permLevel: 0
};

exports.help = {
    name: "advmass",
    description: "assigns the advmass role",
    usage: "advmass"
};