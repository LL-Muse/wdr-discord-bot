const dmDelete = require("../util/dmDelete");
const otherEmbed = require("../util/otherEmbed");
const removeLoc = require("../util/removeLoc");
const config = require("../config.json");
exports.run = function(client, message, messageArg){
    var uswest = message.guild.roles.cache.get(config.uswest);
    if (message.member.roles.cache.has(uswest.id)) {
        removeLoc(message);
        dmDelete(message, config.notifLocRem);
        return;
    }
    removeLoc(message);
    message.member.roles.add(uswest).catch(console.error);
    otherEmbed(message, "uswest", "changed region to");
    dmDelete(message, config.notifLocMod);
};
exports.conf = {
    enabled: true,
    guildOnly: false,
    aliases: [],
    permLevel: 0
};

exports.help = {
    name: "uswest",
    description: "assigns the uswest role",
    usage: "uswest"
};