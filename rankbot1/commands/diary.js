const dmDelete = require("../util/dmDelete");
const rankEmbed = require("../util/rankEmbed");
const removeCox = require("../util/removeCox");
const config = require("../config.json");
const otherEmbed = require("../util/otherEmbed");

exports.run = function(client, message){

    var diary = message.guild.roles.cache.get(config.diary);
    var cox0 = message.guild.roles.cache.get(config.cox0);
    var cox1 = message.guild.roles.cache.get(config.cox1);
    var cox2 = message.guild.roles.cache.get(config.cox2);
    var cox3 = message.guild.roles.cache.get(config.cox3);
    var cox4 = message.guild.roles.cache.get(config.cox4);
    var cox5 = message.guild.roles.cache.get(config.cox5);
	var coxlearner = message.guild.roles.cache.get(config.coxlearner);
    if(message.member.roles.cache.has(cox1.id) || message.member.roles.cache.has(cox2.id) || message.member.roles.cache.has(cox3.id) ||  message.member.roles.cache.has(cox4.id) || message.member.roles.cache.has(cox5.id)){
        message.delete();
        return;
    }
    if (message.member.roles.cache.has(cox0.id)){
        removeCox(message);
        message.member.roles.remove(coxlearner).catch(console.error);
    }
    if(!message.member.roles.cache.has(diary.id)) {
      otherEmbed(message, "diary", "added");
      message.member.roles.add(diary).catch(console.error);
      dmDelete(message, "**__Welcome__**\n" +
        "You have opted for the `Diary` role; this means that you are looking to leech a *Chambers of Xeric* kill count for the *Achievement Diary* step. __Please follow these instructions:__\n" +
        ":one: Copy one of the following messages:\n" +
        "`I am looking to leech a kc for the diary step. Please @ me if I can tag along on your raid and scale it up.`\n" +
        "**OR**, if you are willing to help out in the raid by doing small things like collecting secondaries and vials and herbs, then use the following message\n" +
        "`I am looking to leech a kc for the diary step. Please @ me if I can tag along on your raid and scale it up. I am happy to alt your raid if you give me directions.`\n" +
        ":two: Paste the message into these three channels: (you can click these ->) <#408265938610159616>, <#436565803811209216>, and <#477140139894898688>\n" +
        ":three: If someone invites you on their raid, do as they say. Join their cc, make sure you know how to get to raids, etc.\n" +
        "\n" +
        "__**Guidelines:**__\n" +
        "- Do not die. Stay behind the team at all times and do not enter any boss rooms while the boss is alive.\n" +
        "- You will **not** recieve a split of any unique drops.\n" +
        "- If you recieve a unique drop in your name you are **required** to split it with the team. If you do not split you will be banned and reported to RuneWatch.\n" +
        "- Please type !diary into <#521409656489377813> again once you have obtained your KC. You may use the other commands in the future if you decide you want to learn.");
    }
    else {
      otherEmbed(message, "diary", "removed");
      message.member.roles.remove(diary).catch(console.error);
      dmDelete(message, "**diary** " + config.notifRoleRem);
    }
   
};

exports.conf = {
    enabled: true,
    guildOnly: false,
    aliases: [],
    permLevel: 0
};

exports.help = {
    name: "diary",
    description: "cox achievement diary",
    usage: "diary"
};
