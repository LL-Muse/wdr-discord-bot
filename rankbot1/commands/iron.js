const dmDelete = require("../util/dmDelete");
const otherEmbed = require("../util/otherEmbed");
const config = require("../config.json");
exports.run = function(client, message, messageArg) {
    var iron = message.guild.roles.cache.get(config.iron);
    var ffa = message.guild.roles.cache.get(config.ffa);


    if (message.member.roles.cache.has(iron.id) && message.member.roles.cache.has(ffa.id)) {
        otherEmbed(message, "iron", "removed");
        otherEmbed(message, "ffa", "removed");
        message.member.roles.remove(iron).catch(console.error);
        message.member.roles.remove(ffa).catch(console.error);
        dmDelete(message, config.notifIronRem);
        return;
    }
    if (!message.member.roles.cache.has(iron.id) && message.member.roles.cache.has(ffa.id)) {
        otherEmbed(message, "iron", "added");
        message.member.roles.add(iron).catch(console.error);
        dmDelete(message, config.notifIronAdd);
        return;
    }
    if (message.member.roles.cache.has(iron.id) && !message.member.roles.cache.has(ffa.id)) {
        otherEmbed(message, "iron", "removed");
        message.member.roles.remove(iron).catch(console.error);
        dmDelete(message, config.notifIronRem);
        return;
    }
    otherEmbed(message, "iron", "added");
    otherEmbed(message, "ffa", "added");
    message.member.roles.add(iron).catch(console.error);
    message.member.roles.add(ffa).catch(console.error);
    dmDelete(message, config.notifIronAdd);
};

exports.conf = {
    enabled: true,
    guildOnly: false,
    aliases: [],
    permLevel: 0
};

exports.help = {
    name: "iron",
    description: "assigns the ironman role",
    usage: "iron"
};