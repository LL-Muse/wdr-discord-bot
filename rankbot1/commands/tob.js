const dmDelete = require("../util/dmDelete");
const rankEmbed = require("../util/rankEmbed");
const removeTob = require("../util/removeTob");
const config = require("../config.json");

exports.run = function(client, message, messageArg){
    // KC is not a number error //
        if (isNaN(messageArg) || messageArg === "") {
            dmDelete(message, config.errArgNaN);
            return;
        }

        // KC cannot be negative error //
        if (messageArg < 0) {
            dmDelete(message, config.errKcNegative);
            return;
        }

        var tob0 = message.guild.roles.cache.get(config.tob0);
        var tob1 = message.guild.roles.cache.get(config.tob1);
        var tob2 = message.guild.roles.cache.get(config.tob2);
        var tob3 = message.guild.roles.cache.get(config.tob3);
        var tob4 = message.guild.roles.cache.get(config.tob4);
        var tob5 = message.guild.roles.cache.get(config.tob5);

        errNoChange = config.errNoChange;

        // Theatre of Blood: Tier 0 (New) //
        if (messageArg < 1) {
            var reply = config.notifTobKC + " You are now at the **New** level.";
            reply += "\nOnce you have reached Verzik (the final boss in ToB) for the first time make sure you take a screenshot and go to <#556879235059548161>. This will give you a role that Mentors use to find and notify learners when they are teaching.";
            if (message.member.roles.cache.has(tob0.id)) {
                dmDelete(message, errNoChange[0] + "New" + errNoChange[1]);
                return;
            }
            rankEmbed(message, "Theatre of Blood New");
            removeTob(message);
            message.member.roles.add(tob0).catch(console.error);
            dmDelete(message, reply);
            return;
        }

        // Theatre of Blood: Tier 1 (Learner) //
        if (messageArg < 10) {
            var reply = config.notifTobKC + " You are now at the **Learner** level.";
            if (message.member.roles.cache.has(tob1.id)) {
                dmDelete(message, errNoChange[0] + "Learner" + errNoChange[1]);
                return;
            }
            rankEmbed(message, "Theatre of Blood Learner");
            removeTob(message);
            message.member.roles.add(tob1).catch(console.error);
            dmDelete(message, reply);
            return;
        }

        // Theatre of Blood: Tier 2 (Intermediate) //
        if (messageArg < 50) {
            var reply = config.notifTobKC + " You are now at the **Intermediate** level.";
            if (message.member.roles.cache.has(tob2.id)) {
                dmDelete(message, errNoChange[0] + "Intermediate" + errNoChange[1]);
                return;
            }
            rankEmbed(message, "Theatre of Blood Intermediate");
            removeTob(message);
            let roleV = message.guild.roles.cache.get(config.verzik);
              message.member.roles.remove(roleV).catch(console.error);
            message.member.roles.add(tob2).catch(console.error);
            dmDelete(message, reply);

            return;
        }

        // Theatre of Blood: Tier 3 (Proficient) //
        if (messageArg < 100) {
            var reply = config.notifTobKC + " You are now at the **Proficient** level.";
            if (message.member.roles.cache.has(tob3.id)) {
                dmDelete(message, errNoChange[0] + "Proficient" + errNoChange[1]);
                return;
            }
            rankEmbed(message, "Theatre of Blood Proficient");
            removeTob(message);
            message.member.roles.add(tob3).catch(console.error);
          let roleV = message.guild.roles.cache.get(config.verzik);
            message.member.roles.remove(roleV).catch(console.error);
            dmDelete(message, reply);
            return;
        }

        // Theatre of Blood: Tier 4 (Experienced) //
        if (messageArg < 250) {
            var reply = config.notifTobKC + " You are now at the **Experienced** level.";
            if (message.member.roles.cache.has(tob4.id)) {
                dmDelete(message, errNoChange[0] + "Experienced" + errNoChange[1]);
                return;
            }
            rankEmbed(message, "Theatre of Blood Experienced");
            removeTob(message);
            message.member.roles.add(tob4).catch(console.error);
            let roleV = message.guild.roles.cache.get(config.verzik);
              message.member.roles.remove(roleV).catch(console.error);
            dmDelete(message, reply);
            return;
        }

        // Theatre of Blood: Tier 5 (Elite) //
        if (messageArg >= 250) {
            var reply = config.notifTobKC + " You are now at the **Elite** level.";
            if (message.member.roles.cache.has(tob5.id)) {
                dmDelete(message, errNoChange[0] + "Elite" + errNoChange[1]);
                return;
            }
            rankEmbed(message, "Theatre of Blood Elite");
            removeTob(message);
            message.member.roles.add(tob5).catch(console.error);
            let roleV = message.guild.roles.cache.get(config.verzik);
              message.member.roles.remove(roleV).catch(console.error);
            dmDelete(message, reply);
        }
};
exports.conf = {
    enabled: true,
    guildOnly: false,
    aliases: [],
    permLevel: 0
};

exports.help = {
    name: "tob",
    description: "assigns the tob roles",
    usage: "tob"
};