const dmDelete = require("../util/dmDelete");
const otherEmbed = require("../util/otherEmbed");
const config = require("../config.json");
exports.run = function(client, message, messageArg) {
    var cmintro = message.guild.roles.cache.get(config.cmintro);
    var tierRequirement = "Tier 3";
    if (!message.member.roles.cache.has(cmintro.id)) {
        if (message.member.roles.cache.has(config.cox5) || message.member.roles.cache.has(config.cox4) || message.member.roles.cache.has(config.cox3)) {
            otherEmbed(message, "cmintro", "added");
            message.member.roles.add(cmintro).catch(console.error);
            dmDelete(message, "**cmintro** " + config.notifRoleAdd);
        } else {
            dmDelete(message, config.errKCLow + "**" + tierRequirement + "**");
        }
    } else {
        otherEmbed(message, "cmintro", "removed");
        message.member.roles.remove(cmintro).catch(console.error);
        dmDelete(message, "**cmintro** " + config.notifRoleRem);
    }
};
exports.conf = {
    enabled: true,
    guildOnly: false,
    aliases: [],
    permLevel: 0
};

exports.help = {
    name: "cmintro",
    description: "assigns the cmintro role",
    usage: "cmintro"
};