const Discord = require("discord.js");
const client = new Discord.Client();
const settings = require("./settings.json");
const token = settings.token;
const config = require("./config.json");
const fs = require("fs");


//eventloader is basically the same as commandbot
require("./util/eventLoader")(client);


client.commands = new Discord.Collection();
client.aliases = new Discord.Collection();
fs.readdir("./rankbot1/commands/", (err, files) => {
  if (err) console.error(err);
  console.log(`Loading a total of ${files.length} commands.`);
  files.forEach(f => {
    let props = require(`./commands/${f}`);
    //log(`Loading Command: ${props.help.name}.`);
    client.commands.set(props.help.name, props);
    props.conf.aliases.forEach(alias => {
      client.aliases.set(alias, props.help.name);
    });
  });
});
  client.login(token);

